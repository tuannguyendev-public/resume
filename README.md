# NGUYEN QUOC TUAN

_FULL-STACK DEVELOPER_

<hr/>

## TABLE OF CONTENTS
- [NGUYEN QUOC TUAN](#nguyen-quoc-tuan)
  - [TABLE OF CONTENTS](#table-of-contents)
  - [CONTACT](#contact)
  - [TECHNICAL SKILLS](#technical-skills)
  - [WORK EXPERIENCES](#work-experiences)
    - [Datviet Software (06/2022 - 12/2022)](#datviet-software-062022---122022)
  - [EDUCATION](#education)

<hr/>

## CONTACT
- Name: `Nguyễn Quốc Tuấn`
- Phone: `036 866 7326`
- Email: tuannguyen7326.dev@gmail.com

<hr/>

## TECHNICAL SKILLS

- Main programming languages: `javascript/typescript`, `python`
- Version controls: `git`
- Databases: `mongo`
- Libraries:
  - Backend: `ExpressJS`, `Mongoose`, `jsonwebtoken`
  - Frontend: `ReactJS`, `NextJS`, `MUI`, `redux-toolkit`, `react-query`, `zod`, `styled-components`, etc.
- OS: `windows`, `linux`
- Other: `docker`, `nginx`, `vmware(proxmox)`.
- Cloud: `google cloud`.


## WORK EXPERIENCES

### Datviet Software (06/2022 - 12/2022)

- Position: `Full Stack Developer`
- Address: Phường Long Thạnh Mỹ, TP. Thủ Đức, HCM
- Project: Checkee
  - Describe: 
    - Retrieve information on the origin of agricultural products.
  - Responsibilities:
    - Design database schema and implement backend for checkee service.
    - Build and deploy app with `docker` and `nginx`.
    - Support frontend team.
  - Team size: 6
  - Technical:
    - RestfulAPI
    - Backend:
      - Programming language: `typescript`
      - Database: `mongo`
      - Frameworks: `expressJs`, `mongoose`, `jsonwebtoken`, etc.
    - Frontend:
      - Mobile: `react-native`
      - Web: `reactjs`, `redux-toolkit`


## EDUCATION

- [JavaScript Algorithms and Data Structures](https://freecodecamp.org/certification/fcc5da6fdec-e7d6-4221-8956-4f8fc598fc45/javascript-algorithms-and-data-structures)
- [Industrial University of Ho Chi Minh City](https://iuh.edu.vn/en)
  - Computer science
  - Status: Completed all subjects.