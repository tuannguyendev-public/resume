# NGUYEN QUOC TUAN

_FULL-STACK DEVELOPER_

<hr/>

## MỤC LỤC
- [NGUYEN QUOC TUAN](#nguyen-quoc-tuan)
  - [MỤC LỤC](#mục-lục)
  - [LIÊN HỆ](#liên-hệ)
  - [KỸ NĂNG](#kỹ-năng)
  - [KINH NGHIỆM LÀM VIỆC](#kinh-nghiệm-làm-việc)
    - [Datviet Software (06/2022 - 12/2022)](#datviet-software-062022---122022)
  - [HỌC VẤN](#học-vấn)

<hr/>

## LIÊN HỆ
- Tên: `Nguyễn Quốc Tuấn`
- Điện thoại: `036 866 7326`
- Email: tuannguyen7326.dev@gmail.com

<hr/>

## KỸ NĂNG

- Ngôn ngữ lập trình chính: `javascript/typescript`
- Quản lý phiên bản: `git`
- Databases: `mongo`
- Thư viện:
  - Backend: `ExpressJS`, `Mongoose`, `jsonwebtoken`
  - Frontend: `ReactJS`, `NextJS`, `MUI`, `redux-toolkit`, `react-query`, `zod`, `styled-components`, etc.
- OS: `windows`, `linux`
- Khác: `docker`, `nginx`, `vmware(proxmox)`.
- Cloud: `google cloud`.


## KINH NGHIỆM LÀM VIỆC

### Datviet Software (06/2022 - 12/2022)

- Vị trí: `Full Stack Developer`
- Địa chỉ: Phường Long Thạnh Mỹ, TP. Thủ Đức, HCM
- Dự án: `Checkee`
  - Mô tả: 
    - Truy xuất thông tin nguồn gốc nông sản.
  - Trách nhiệm:
    - Thiết kế database schema và triển khai backend
    - Build và deploy dự án với `docker` và `nginx`.
    - Hỗ trợ team frontend
  - Team size: 6
  - Technical:
    - RestfulAPI
    - Backend:
      - Ngôn ngữ lập trình: `typescript`
      - Database: `mongo`
      - Frameworks: `expressJs`, `mongoose`, `jsonwebtoken`, etc.
    - Frontend:
      - Mobile: `react-native`
      - Web: `reactjs`, `redux-toolkit`


## HỌC VẤN
- [JavaScript Algorithms and Data Structures](https://freecodecamp.org/certification/fcc5da6fdec-e7d6-4221-8956-4f8fc598fc45/javascript-algorithms-and-data-structures)
  - [Verify this certification at https://freecodecamp.org/certification/fcc5da6fdec-e7d6-4221-8956-4f8fc598fc45/javascript-algorithms-and-data-structures](https://freecodecamp.org/certification/fcc5da6fdec-e7d6-4221-8956-4f8fc598fc45/javascript-algorithms-and-data-structures)
- [ Đại học Công Nghiệp TP.HCM ](https://iuh.edu.vn)
  - Trạng thái: Đã hoàn thành tất cả các môn học.